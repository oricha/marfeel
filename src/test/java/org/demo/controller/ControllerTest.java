package org.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.demo.model.UrlExam;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class ControllerTest {
	
	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;
	
	@Before
	public void before() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}
	
	@Test
	public void upAndRunTest() throws Exception{
		this.mockMvc.perform(MockMvcRequestBuilders.get("/"))
		.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void nominalCaseTest() throws Exception{
		UrlExam stub = new UrlExam("abcnews.go.com", 0L);
		List<UrlExam> list = new ArrayList<UrlExam>();
		list.add(stub);
		Gson gson = new Gson();
	    String json = gson.toJson(list);
	    
		this.mockMvc.perform(MockMvcRequestBuilders.post("/marfeelizator")
		.contentType(MediaType.APPLICATION_JSON).content(json))
		.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void collectionCaseTest() throws Exception{
		List<UrlExam> list =stubList();
		Gson gson = new Gson();
	    String json = gson.toJson(list);
	    
		this.mockMvc.perform(MockMvcRequestBuilders.post("/marfeelizator")
		.contentType(MediaType.APPLICATION_JSON).content(json))
		.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	private List<UrlExam> stubList(){
		List<UrlExam> list = new ArrayList<UrlExam>();
		list.add(new UrlExam("centrallecheraasturiana.es", 834987L));
		list.add(new UrlExam("guiafull.com", 54987L));
		list.add(new UrlExam("leasing4business.co.uk", 45667L));
		list.add(new UrlExam("abcnews.go.com", 54987L));
		list.add(new UrlExam("ayto-arganda.es", 6787L));
		list.add(new UrlExam("fcc.es", 456727L));
		return list;
	}
}
