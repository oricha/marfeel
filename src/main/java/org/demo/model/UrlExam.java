package org.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UrlExam {
	
	@Id @GeneratedValue(strategy= GenerationType.AUTO)
	private long id;
	private String url;
	private Long rank;
	
	public UrlExam(){};
	
	
	public UrlExam(String url, Long rank) {
		super();
		this.url = url;
		this.rank = rank;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Long getRank() {
		return rank;
	}
	public void setRank(Long rank) {
		this.rank = rank;
	}
	
	
}
