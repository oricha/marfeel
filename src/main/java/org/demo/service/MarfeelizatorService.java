package org.demo.service;

import java.io.IOException;

import org.demo.model.UrlExam;
import org.demo.repository.UrlRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("marfeelService")
@Transactional
public class MarfeelizatorService {

	private static final Logger log = LoggerFactory.getLogger(MarfeelizatorService.class);
	@Autowired
	UrlRepository repository;
	private static String HTTP = "http://";

	public boolean isMarfeelizable(UrlExam url) {
		Document doc;
		try {
			doc = Jsoup.connect(HTTP + url.getUrl()).get();
			String title = doc.title();

			if (title.contains("news") || title.contains("News") || title.contains("noticias")
					|| title.contains("Noticias")) {
				repository.save(url);
				log.info("Si "+url.getUrl());
				return true;
			}
		} catch (IOException e) {
			log.error("HTTP error fetching URL: " + url.getUrl());
		}
		log.info("No "+url.getUrl());
		return false;
	}

}
