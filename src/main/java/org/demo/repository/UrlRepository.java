package org.demo.repository;

import org.demo.model.UrlExam;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UrlRepository extends JpaRepository<UrlExam, Long> {

}
