package org.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarfeelApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarfeelApplication.class, args);
	}
}
