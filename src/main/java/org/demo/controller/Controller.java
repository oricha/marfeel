package org.demo.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.demo.model.UrlExam;
import org.demo.service.MarfeelizatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	private static final Logger log = LoggerFactory.getLogger(Controller.class);
	@Autowired
	MarfeelizatorService service;

	@RequestMapping(value = "/marfeelizator", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity setTransaction(@RequestBody UrlExam[] urlList) throws IOException {

		Set<UrlExam> setList = new HashSet<UrlExam>(Arrays.asList(urlList));
		setList.parallelStream().forEach((item) -> {
			service.isMarfeelizable(item);
		});
		return new ResponseEntity(HttpStatus.OK);
	}

	public class Request {

		List<UrlExam> items;

		public List<UrlExam> getItems() {
			return items;
		}

		public void setItems(List<UrlExam> items) {
			this.items = items;
		}
	}
}
